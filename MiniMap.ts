class MiniMap {
    private miniMapW: number
    private miniMapH: number
    private miniMapCuadrado: number
    private proporcionX: number
    private proporcionY: number
    constructor (w: number, h: number, pequeno: number, fondoW, fondoH, width, height) {
        this.miniMapW = w
        this.miniMapH = h
        this.miniMapCuadrado = pequeno
        this.proporcionX = (this.miniMapW-this.miniMapCuadrado)/(fondoW-width)
        this.proporcionY = (this.miniMapH-this.miniMapCuadrado)/(fondoH-height)
    }

    public drawMiniMap(ctx, h, deltax, deltay) {
        ctx.beginPath()
        ctx.rect(0, h-150, 200, 150)
        ctx.fillStyle = 'grey'
        ctx.fill()
        ctx.strokeStyle = 'yellow'
        ctx.stroke()
        ctx.beginPath()
        ctx.rect(deltax*this.proporcionX,(h-this.miniMapH)+deltay*this.proporcionY,this.miniMapCuadrado, this.miniMapCuadrado)
        ctx.strokeStyle = 'red'
        ctx.stroke()
    }

    public esSobreMiniMap(x, y, h) {
        if (x>0 && x<this.miniMapW && y>h-this.miniMapH && y<h) {
            return true
        }
        else {
            return false
        }
    }
    
}
