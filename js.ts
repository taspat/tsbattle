///<reference path="Game.ts"/>
declare var io
// var socket = io.connect('http://localhost:9000')
var socket = io.connect('http://52.28.107.172:9000')

//main method :D
window.onload = function () {
    var miTipoNave: string
    var enemyTipoNave: string
    var game: Game
    var puedeAtacar: boolean = true
    document.getElementById('elegirNave').onclick = function (e) {
        var tipoNave = e.target.id
        if (tipoNave === 'elegirNave') return
        miTipoNave = tipoNave 
        socket.emit('elegirNave', tipoNave)
        document.getElementById('elegirNave').style.display = 'none'
        document.getElementById('msgWaiting').style.display = 'block'
    }
    
    //------------------ bunch of listeners ------------------------------------
    
    socket.on('startGame', function(data) {
        enemyTipoNave = data
        game = new Game(miTipoNave, enemyTipoNave)
        document.getElementById('msgWaiting').style.display = 'none'
        game.startGame()

        socket.on('leftClick', function (data) { // atacar
            var x = data[0] - game.getDeltaX()
            var y = data[1] - game.getDeltaY()
            if (data[2] === game.getMiNave().getId()) {
                if (game.esSobreMiniMapa(x, y)) {
                    return
                }
                game.limpiarComandosPendientes()
                game.shootInmediatamente(x, y, game.getEnemyNave())
            }
            else {
                game.limpiarComandosPendientesEnemy()
                game.shootInmediatamenteEnemy(x, y, game.getMiNave())
            }
        })
        socket.on('Shift-LeftClick', function (data) {
            var x = data[0] - game.getDeltaX()
            var y = data[1] - game.getDeltaY()
            if (data[2] === game.getMiNave().getId()) {
                if (game.esSobreMiniMapa(x, y)) {
                    return
                }
                // console.log('recibido shift left')
                game.anadirShoot(x, y,game.getEnemyNave())
            }
            else {
                game.anadirShootEnemy(x,y,game.getMiNave())
            }
        })

        socket.on('rightClick', function (data) { // andar
            var x = data[0] - game.getDeltaX()
            var y = data[1] - game.getDeltaY()
            if (data[2] === game.getMiNave().getId()) {
                if (game.esSobreMiniMapa(x, y)) {
                    return
                }
                game.limpiarComandosPendientes()
                game.moveInmediatamente(x, y)
            }
            else {
                game.limpiarComandosPendientesEnemy()
                game.moveInmediatamenteEnemy(x,y)
            }
        })

        socket.on('Shift-RightClick', function (data) {
            var x = data[0] - game.getDeltaX()
            var y = data[1] - game.getDeltaY()
            if (data[2] === game.getMiNave().getId()) {
                if (game.esSobreMiniMapa(x, y)) {
                    return
                }
                game.anadirMove(x, y)
            }
            else {
                game.anadirMoveEnemy(x,y)
            }
        })

        socket.on('miId', function (data) {
            game.getMiNave().setId(data)
        })
        socket.on('enemyId', function (data) {
            game.getEnemyNave().setId(data)
        })
        document.onmousemove = function (e) {
            game.moverPantalla(e)
        }

        window.oncontextmenu = function () {
            return false
        }


        document.onmouseup = function(e) {
            var x = e.pageX + game.getDeltaX()
            var y = e.pageY + game.getDeltaY()
            var qBotton = e.which
            var ataca = (clickHexo) => {
                    puedeAtacar = false
                    socket.emit(clickHexo, [x, y]);
                    setTimeout(()=>{puedeAtacar = true},3000) 
            }
            if (qBotton === 3 && e.shiftKey) {
                socket.emit('Shift-RightClick', [x, y]);
            }
            else if (qBotton === 1 && e.shiftKey) {
                if (puedeAtacar){
                    ataca('Shift-LeftClick')
                }
                else {
                    // console.log('tienes que esperar')
                }
            }
            else if (qBotton === 3) { // el derecho de raton
                socket.emit('rightClick', [x, y]);
            }
            else if (qBotton === 1) {
                if (puedeAtacar){
                    ataca('leftClick')
                }
                else {
                    // console.log('tienes que esperar')
                }
            }
        }
    })
}


