function toDeg(p) {
    if (p < 0) {
        return 360 + (p * (180/Math.PI))
    }
    return p * (180/Math.PI)
}
interface IVisitor {
    accept(bala: IBala)
}

class Nave implements IVisitor {
    private vida: number
    private state: IStateNave
    private x:number
    private y:number
    private img:HTMLImageElement
    private imgSrc:string[]
    private actualImage:number
    private id:number
    private width:number
    private height:number
    private anguloViejo:number
    private anguloNuevo:number
    private centroX:number
    private centroY:number
    private timerAngulo:number
    private T: number
    private bala: IBala
    private rango: number
    private speed: number
    constructor (w, h, x, y, speed, rango, vida) {
        this.state = StopStateNave.getSingleton()
        this.x=x
        this.y=y
        this.speed = speed
        this.vida = vida
        this.imgSrc = []
        this.actualImage = 0
        this.rango = rango
        this.width = w
        this.height = h
        this.anguloViejo = 0
        this.anguloNuevo = 0
        this.centroX = this.x+this.width/2 //buwo 188/2
        this.centroY = this.y+this.height/2
        this.timerAngulo = 0
        this.id = 0
        this.img = new Image()
        this.T = 16
    }
    public accept(bala: IBala) {}
    public animate() {
        this.state.animate(this)
    }
    public getState() {
        return this.state
    }
    public setState(s: IStateNave) {
        this.state = s
    }
    public getVida(){
        return this.vida
    }
    public setVida(x: number){
        this.vida = x
    }
    public drawMiBala(ctx) {
        if (this.getState() !== ShootingStateNave.getSingleton() || this.bala === null || this.bala.esFin()) {
            return
        }
        else {
            this.bala.draw(ctx)
        }
    }
    public shoot(x, y,enemy) {
        // this.setState(ShootingStateNave.getSingleton())
        if (Math.floor((Math.random() * 5) + 1) === 5) {
            this.bala = new SuperBala(new Bala(this.centroX, this.centroY, 10,enemy))
        }
        else {
            this.bala = new Bala(this.centroX, this.centroY, 10,enemy)
        }
        var lambda = (a,b) => {
            this.bala.setX(this.bala.getX() + a)
            this.bala.setY(this.bala.getY() + b)
        }
        this.helper(x,y,20,0,0,
                    lambda,
                    (a) => {
                        if (this.bala.esFin()) {
                            return false
                        }
                        else if (a > this.rango){
                            this.bala.setFin()
                            return false
                        }
                        else if (this.bala.xokaEnemy()) {
                            this.bala.setFin()
                            enemy.accept(this.bala) 
                            return false
                        }
                        return true
                    }
                   )
        
    }
    public nextImage() {
        if (this.actualImage === this.imgSrc.length - 1) {
            this.img.src = this.imgSrc[0]
            this.actualImage = 0
        }
        else {
            this.img.src = this.imgSrc[this.actualImage + 1]
            this.actualImage += 1
        }
    }
    public setStopImage(){
        this.actualImage = 0
        this.img.src = this.imgSrc[0]
    }
    public setImgSrc(arr: string[]) {
        this.imgSrc = arr
    }
    public setCentroX(i: number) {
        this.centroX = i
    }
    public getCentroX() {
        return this.centroX
    }
    public setCentroY(i: number) {
        this.centroY = i
    }
    public getCentroY() {
        return this.centroY
    }
    public getId() {
        return this.id
    }
    public setId(id: number) {
        return this.id = id
    }

    public drawNaveFinal(ctx: CanvasRenderingContext2D) {
        ctx.save()
        ctx.translate(this.centroX,this.centroY)
        ctx.rotate(toRad(this.anguloViejo))
        this.animate()
        ctx.drawImage(this.img,-this.width/2,-this.height/2)
        ctx.restore()

        function toRad(p) {
            return p * (Math.PI/180)
        }
    }


    public helper(x,y,speed,hastaX,hastaY, f, cond){
        clearInterval(this.timerAngulo)
        var delta = 5
        var dir = 1
        var vectorX
        var vectorY
        var deltaVectorX
        var deltaVectorY
        var recorrido = 0
        var r
        var fx = Math.abs(this.centroX - x)
        var fy = Math.abs(this.centroY - y)
        var skirtumas
        if (fx > fy) skirtumas = fy/fx 
        else skirtumas = fx/fy
        var vel = speed
        this.anguloNuevo = toDeg(Math.atan2(x-this.centroX,-(y-this.centroY)))
        var v = this.anguloViejo
        var n = this.anguloNuevo
        if (n > v) {
            if (360-n+v < n-v) {
                dir = -1
                r = 360-n+v
            }
            else {
                dir = 1
                r = n-v
            }
        }
        else {
            if (360-v+n < v-n) {
                dir = 1
                r = 360-v+n
            }
            else {
                dir = -1
                r = v-n
            }
        }
        
        if (x >= this.centroX){
            vectorX = 1
            deltaVectorX = x-this.centroX
        }
        else {
            vectorX = -1
            deltaVectorX = this.centroX - x
        }

        if (y >= this.centroY) {
            vectorY = 1
            deltaVectorY = y -this.centroY
        }
        else {
            vectorY = -1
            deltaVectorY = this.centroY - y
        }
        
        var haCumplidoDistancia = false
        var haCumplidoElAngulo = false
        this.timerAngulo = setInterval(() => {
            if (r <= delta) {
                if (r === delta) {
                    delta = 0
                }
                else delta = r
            }
            if (fx > fy) {
                if (cond(recorrido) && deltaVectorX - hastaX > 1) {
                    f(vel*vectorX, vel*skirtumas*vectorY)
                    deltaVectorX -= vel
                    recorrido += vel
                }
                else {
                    haCumplidoDistancia = true
                }
            }
            else {
                if (cond(recorrido) && deltaVectorY - hastaY > 1) {
                    f(vel * skirtumas * vectorX, vel*vectorY)
                    deltaVectorY -= vel
                    recorrido += vel
                }
                else {
                    haCumplidoDistancia = true
                }
            }
            if (Math.floor(this.anguloNuevo) !== Math.floor(this.anguloViejo)) {
                r -= delta
                this.anguloViejo += delta*dir
                if (this.anguloViejo < 0) this.anguloViejo += 360
                else if (this.anguloViejo > 360) this.anguloViejo -= 360
            }
            else {
                haCumplidoElAngulo = true 
            }

            if (haCumplidoElAngulo && haCumplidoDistancia) {
                this.setState(StopStateNave.getSingleton())
                clearInterval(this.timerAngulo)
            }
        }, this.T)
        
    }
    public move(x,y) {

        if (!x && !y) return
        var lambda = (a,b) => {
            this.centroX += a
            this.centroY += b
        }
        this.helper(x,y,this.speed,this.width/2,this.height/2,
                    lambda,
                    (a) => {
                        return true
                    }
                   )
    }

}



class Nave1 extends Nave {
    constructor() {
        super(129,186, 50, 50, 3, 400, 150)
        this.setImgSrc(['images/nave10.png'])
        // this.setVida(120)
    }
    public accept(bala:IBala){
        bala.visitNave1(this)
    }
}

class Nave2 extends Nave {
    constructor() {
        super(182, 168, 600, 400, 4, 420, 130)
        this.setImgSrc(['images/nave20.png', 'images/nave21.png','images/nave25.png'])
        // this.setVida(100)
    }
    public accept(bala:IBala){
        bala.visitNave2(this)
    }
}

class Nave3 extends Nave {
    constructor() {
        super(89,103, 50, 400, 5, 500, 100)
        // this.setVida(80)
        this.setImgSrc(['images/nave30.png'])
    }
    public accept(bala:IBala){
        bala.visitNave3(this)
    }
}

class Nave4 extends Nave {
    constructor() {
        super(105,101,600,400, 6, 550, 80)
        this.setImgSrc(['images/nave40.png'])
        // this.setVida(60)
    }
    public accept(bala:IBala){
        bala.visitNave4(this)
    }
}

class FactoryNaves {
    public getNave(tipo: string) {
        if (tipo === 'nave1') {
            return new Nave1()
        }
        else if (tipo === 'nave2') {
            return new Nave2()
        }
        else if (tipo === 'nave3') {
            return new Nave3()
        }
        else if (tipo === 'nave4') {
            return new Nave4()
        }
    }
}


interface IStateNave {
    animate(n: Nave)
}


class ShootingStateNave implements IStateNave{
    private static singleton = new ShootingStateNave()
    constructor() {
        if(ShootingStateNave.singleton){
            throw new Error('Es un singleton, usa otra getInstancia() para obtener su referencia')
        }
        ShootingStateNave.singleton = this
    }
    public static getSingleton() {
        return ShootingStateNave.singleton
    }
    animate(n: Nave) {
        n.nextImage()
    }
}
class MovingStateNave implements IStateNave{
    private static singleton = new MovingStateNave()
    constructor() {
        if(MovingStateNave.singleton){
            throw new Error('Es un singleton, usa otra getInstancia() para obtener su referencia')
        }
        MovingStateNave.singleton = this
    }
    public static getSingleton() {
        return MovingStateNave.singleton
    }
    animate(n: Nave) {
        n.nextImage()
    }
}
class StopStateNave implements IStateNave{
    private static singleton = new StopStateNave()
    constructor() {
        if(StopStateNave.singleton){
            throw new Error('Es un singleton, usa otra getSingleton() para obtener su referencia')
        }
        StopStateNave.singleton = this
    }
    public static getSingleton() {
        return StopStateNave.singleton
    }
    
    animate(n: Nave) {
        n.setStopImage()
    }
}
