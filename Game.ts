///<reference path="Bala.ts"/>
///<reference path="naveModule.ts"/>
///<reference path="Assets.ts"/>
///<reference path="MiniMap.ts"/>
///<reference path="CommandPattern.ts"/>
class Game {
    public canvas: HTMLCanvasElement
    public ctx: CanvasRenderingContext2D
    private w: number //width of canvas
    private h: number //height of canvas
    private dirx: number
    private diry: number
    private deltax: number
    private deltay: number
    private zoomX: number
    private zoomY: number
    private fondoW: number
    private fondoH: number
    private ratonMargin: number
    private minimap: MiniMap
    private T:number //el periodo de la frencuenta 60 frame/second
    private animationLock: boolean 
    private animationTimer: number
    private background: HTMLImageElement 
    private timerDireccion: number
    private miNave: Nave 
    private enemyNave: Nave
    private timerGame: number
    private comandosPendientes: ComandoControl
    private comandosPendientesEnemy: ComandoControl
    private factoriaDeNaves: FactoryNaves

    constructor(miTipoNave: string, enemyTipoNave: string) {
        // console.log('konstructorius su miTipo ' + miTipoNave + ' enemy ' + enemyTipoNave)
        this.canvas = document.createElement('canvas')
        this.ctx = this.canvas.getContext("2d")
        document.body.appendChild(this.canvas)
        this.w = this.canvas.width = window.innerWidth
        this.h = this.canvas.height = window.innerHeight
        // console.log('constructorius')
        this.dirx = 0
        this.diry = 0
        this.deltax = 0
        this.deltay = 0
        this.zoomX = 5
        this.zoomY = 5
        this.fondoW = 3000
        this.fondoH = 1500
        this.ratonMargin = 20
        this.minimap = new MiniMap(200, 150, 50, this.fondoW, this.fondoH, this.w, this.h)
        this.T = 16 //el periodo de la frencuenta 60 frame/second
        this.animationLock = false
        this.animationTimer 
        this.background  = new Image() 
        // this.canvas.id = 'canvas' //css id for canvas
        this.background.src = 'images/fondo2.png'
        this.timerDireccion
        this.factoriaDeNaves = new FactoryNaves()
        this.miNave = this.factoriaDeNaves.getNave(miTipoNave)
        this.enemyNave = this.factoriaDeNaves.getNave(enemyTipoNave)
        // this.miNave = new Nave(miTipoNave)
        // this.enemyNave = new Nave(enemyTipoNave)
        this.comandosPendientes = new ComandoControl()
        this.comandosPendientesEnemy = new ComandoControl()
    }
    public getMiniMap() {
        return this.minimap
    }
    public getMiNave() {
        return this.miNave
    }    
    public getEnemyNave() {
        return this.enemyNave
    }    
    public getCanvas() {
        return this.canvas
    }

    public setCanvas(c: HTMLCanvasElement) {
        this.canvas = c
    }

    public getCtx() {
        return this.ctx
    }

    public esSobreMiniMapa(x, y) {
        this.getMiniMap().esSobreMiniMap(x, y, this.h)
    }

    public drawMiNave() {
        // console.log('mi vida ' + this.getMiNave().getVida())
        this.miNave.drawNaveFinal(this.ctx)
    }
    
    public drawEnemyNave() {
        // console.log('su vida ' + this.getMiNave().getVida())
        this.enemyNave.drawNaveFinal(this.ctx)
    }
    public anadirMove(x, y){
        this.comandosPendientes.add(new ComandoMover(this.getMiNave(), x, y))
    }
    public anadirMoveEnemy(x, y){
        this.comandosPendientesEnemy.add(new ComandoMover(this.getEnemyNave(), x, y))
    }
    public anadirShoot(x, y,destinyNave){
        this.comandosPendientes.add(new ComandoAtacar(this.getMiNave(), x, y, destinyNave))
    }
    public anadirShootEnemy(x, y,destinyNave){
        this.comandosPendientesEnemy.add(new ComandoAtacar(this.getEnemyNave(), x, y, destinyNave))
    }
    public moveInmediatamente(x,y) {
        new ComandoMover(this.getMiNave(), x, y).ejecutar()
    }
    public moveInmediatamenteEnemy(x,y) {
        new ComandoMover(this.getEnemyNave(), x, y).ejecutar()
    }
    public shootInmediatamente(x,y,destinyNave) {
        new ComandoAtacar(this.getMiNave(), x, y, destinyNave).ejecutar()
    }
    public shootInmediatamenteEnemy(x,y, destinyNave) {
        new ComandoAtacar(this.getEnemyNave(), x, y, destinyNave).ejecutar()
    }
    public limpiarComandosPendientes() {
        this.comandosPendientes.reset()
    }
    public limpiarComandosPendientesEnemy() {
        this.comandosPendientesEnemy.reset()
    }
    public checkForActions(){
        if (this.getMiNave().getState() === StopStateNave.getSingleton() && !this.comandosPendientes.esVacio()) {
            // this.getMiNave().setState(MovingStateNave.getSingleton())
            this.comandosPendientes.ejecutarUno()
        }
        if (this.getEnemyNave().getState() === StopStateNave.getSingleton() && !this.comandosPendientesEnemy.esVacio()) {
            // this.getEnemyNave().setState(MovingStateNave.getSingleton())
            this.comandosPendientesEnemy.ejecutarUno()
        }
    }

    public drawBackground(){
        this.w = this.canvas.width = this.canvas.width
        this.h = this.canvas.height = this.canvas.height
        this.ctx.drawImage(this.background,this.deltax,this.deltay,this.w, this.h, 0,0, this.w, this.h)
    }

    public drawMiniMap() {
        this.minimap.drawMiniMap(this.ctx, this.h, this.deltax, this.deltay)
    }
    
    public moverPantalla(e) {
        //si esta encima del minimapa
        var x = e.pageX
        var y = e.pageY
        //x cordenadas
        if (x > this.ratonMargin && x < this.w-this.ratonMargin && y > this.ratonMargin && y < this.h-this.ratonMargin) {
            clearInterval(this.animationTimer)
            this.animationLock = false
        }
        else {
            if (!this.animationLock){
                this.animationLock = true
                var that = this
                this.animationTimer = setInterval(function () {
                    that.hacerCambioRelativos()
                },that.T)
            }
            //cada lado
            if (x < this.ratonMargin) {
                this.dirx = -1
            } 
            else if (x > this.w-this.ratonMargin) {
                this.dirx = 1
            } 
            else if (x > this.ratonMargin) {
                this.dirx = 0
            }

            if (y < this.ratonMargin) {
                this.diry = -1
            } 
            else if (y > this.h-this.ratonMargin) {
                this.diry = 1
            } 
            else if (y > this.ratonMargin) {
                this.diry = 0
            } 
        }
    }
    public getDeltaX() {
        return this.deltax
    }
    public getDeltaY() {
        return this.deltay
    }
    public hacerCambioRelativos() {
        // console.log('cia yra this ' + this.miNave)
        var aumentarX, aumentarY
        if (this.dirx !== 0) {
            aumentarX = this.fondoW-this.w-this.deltax
            if (this.deltax <= this.zoomX && this.dirx === -1) {
                this.deltax -= this.deltax
                // this.miNave.centroX += this.deltax
                this.miNave.setCentroX(this.miNave.getCentroX() + this.deltax)
                this.enemyNave.setCentroX(this.enemyNave.getCentroX() + this.deltax)
                this.comandosPendientes.arreglarDeltaX(this.deltax)
                this.comandosPendientesEnemy.arreglarDeltaX(this.deltax)
                // enemyNave.centroX += deltax
            }
            else if (this.zoomX >= aumentarX && this.dirx === 1) {
                this.deltax += aumentarX
                // this.miNave.centroX -= aumentarX
                this.miNave.setCentroX(this.miNave.getCentroX() - aumentarX)
                this.enemyNave.setCentroX(this.enemyNave.getCentroX() - aumentarX)
                this.comandosPendientes.arreglarDeltaX(aumentarX)
                this.comandosPendientesEnemy.arreglarDeltaX(aumentarX)
                // enemyNave.centroX -= aumentarX
            }
            else {
                this.deltax += this.zoomX * this.dirx
                // this.miNave.centroX -= this.zoomX * this.dirx
                // console.log(this.miNave)
                this.miNave.setCentroX(this.miNave.getCentroX() - this.zoomX * this.dirx)
                this.enemyNave.setCentroX(this.enemyNave.getCentroX() - this.zoomX * this.dirx)
                this.comandosPendientes.arreglarDeltaX(this.zoomX * this.dirx)
                this.comandosPendientesEnemy.arreglarDeltaX(this.zoomX * this.dirx)
                // enemyNave.centroX -= zoomX*dirx
            }
        }
        if (this.diry !== 0) {
            aumentarY = this.fondoH-this.h-this.deltay
            if (this.deltay <= this.zoomY && this.diry === -1) {
                this.deltay -= this.deltay
                // this.miNave.centroY += this.deltay
                this.miNave.setCentroY(this.miNave.getCentroY() + this.deltay)
                this.enemyNave.setCentroY(this.enemyNave.getCentroY() + this.deltay)
                this.comandosPendientes.arreglarDeltaY(this.deltay)
                this.comandosPendientesEnemy.arreglarDeltaY(this.deltay)
                // enemyNave.centroY += deltay
            }
            else if (this.zoomY >= aumentarY && this.diry === 1) {
                this.deltay += aumentarY
                // this.miNave.centroY -= aumentarY
                this.miNave.setCentroY(this.miNave.getCentroY() - aumentarY)
                this.enemyNave.setCentroY(this.enemyNave.getCentroY() - aumentarY)
                this.comandosPendientes.arreglarDeltaY(aumentarY)
                this.comandosPendientesEnemy.arreglarDeltaY(aumentarY)
                // enemyNave.centroY -= aumentarY
            }
            else {
                this.deltay += this.zoomY * this.diry
                // this.miNave.centroY -= this.zoomY * this.diry
                this.miNave.setCentroY(this.miNave.getCentroY() - this.zoomY * this.diry)
                this.enemyNave.setCentroY(this.enemyNave.getCentroY() - this.zoomY * this.diry)
                this.comandosPendientes.arreglarDeltaY(this.zoomY * this.diry)
                this.comandosPendientesEnemy.arreglarDeltaY(this.zoomY * this.diry)
                // enemyNave.centroY -= zoomY*diry
            }
        }
    }
    public drawMiBala(){
        this.getMiNave().drawMiBala(this.ctx)
    }
    public drawEnemyBala(){
        this.getEnemyNave().drawMiBala(this.ctx)
    }
    public mostrarPuntos(){
        // this.ctx.font="30px Verdana"
        this.ctx.font="15px Georgia"
        this.ctx.fillStyle = 'green'
        this.ctx.fillText("Mi Vida: " + this.getMiNave().getVida(),0,15)
        this.ctx.fillText("Su Vida: " + this.getEnemyNave().getVida(),0,30)
    }
    
    public startGame() {
        var assets = new Assets()
        assets.preload()
        var preloadStuff = () => {
            if (assets.isAllLoaded()) {
                // document.body.appendChild(game.getCanvas())
                this.render()
            }
            else {
                setTimeout(preloadStuff, 50)
            }
        }
        preloadStuff()
    }
    


    public render() {
        this.timerGame = setInterval(() => {
            this.drawBackground()
            this.drawMiNave()
            this.drawEnemyNave()
            this.drawMiniMap()
            this.checkForActions()
            this.drawMiBala()
            this.drawEnemyBala()
            this.mostrarPuntos()
        },this.T)
    }
    
}
