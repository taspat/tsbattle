interface IComando {
    ejecutar()
    setx(x:number)
    getx()
    sety(x:number)
    gety()
}


class ComandoMover implements IComando{
    private nave: Nave
    private x
    private y
    constructor (n: Nave, x: number, y: number) {
        this.nave = n 
        this.x = x
        this.y = y
    }
    public ejecutar() {
        if(this.nave.getState() === ShootingStateNave.getSingleton()) {
            console.log('taip jis shoting state dabar')
            return
        }
        this.nave.setState(MovingStateNave.getSingleton())
        this.nave.move(this.x, this.y)
    }
    public getx(){
        return this.x
    }
    public setx(x:number){
        this.x = x
    }
    public gety(){
        return this.y
    }
    public sety(y:number){
        this.y = y
    }
} 

class ComandoAtacar implements IComando{
    private nave
    private x
    private y
    private enemy
    constructor (n: Nave, x: number, y: number, enemy:Nave) {
        this.nave = n 
        this.x = x
        this.y = y
        this.enemy = enemy
    }
    public ejecutar() {
        this.nave.setState(ShootingStateNave.getSingleton())
        this.nave.shoot(this.x, this.y,this.enemy)
    }
    public getEnemy(){
        return this.enemy
    }
    public getx(){
        return this.x
    }
    public setx(x:number){
        this.x = x
    }
    public gety(){
        return this.y
    }
    public sety(y:number){
        this.y = y
    }
}

class ComandoControl {
    private commandList: IComando[] = []
    private terminado: boolean

    constructor () {
        this.commandList = []
    }
    public add(c: IComando) {
        this.commandList.push(c)
    }
    public reset() {
        this.commandList = []
    }
    public esVacio() {
        return this.commandList.length === 0
    }
    public ejecutarUno() {
        this.commandList.shift().ejecutar()
    }
    /* estos metodos abajo se encargar de ajustar las coerdanadas cuando los jugadores mueven el mapa grande
*/
    public arreglarDeltaX(x: number) {
        this.commandList.forEach((a,b) => {
            a.setx(a.getx()-x)
        })
    }
    public arreglarDeltaY(y: number) {
        this.commandList.forEach((a,b) => {
            a.sety(a.gety()-y)
        })
    }
}
