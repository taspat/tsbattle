var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Bala = (function () {
    function Bala(x, y, dano, n) {
        this.x = x;
        this.y = y;
        this.dano = dano;
        this.fin = false;
        this.enemy = n;
    }
    Bala.prototype.getDano = function () {
        return this.dano;
    };
    Bala.prototype.setDano = function (d) {
        this.dano = d;
    };
    Bala.prototype.xokaEnemy = function () {
        var dx = this.x - this.enemy.getCentroX();
        var dy = this.y - this.enemy.getCentroY();
        return dx * dx + dy * dy < 3000;
    };
    Bala.prototype.draw = function (ctx) {
        ctx.beginPath();
        ctx.arc(this.getX(), this.getY(), 5, 0, 2 * Math.PI);
        ctx.fillStyle = 'yellow';
        ctx.stroke();
        ctx.fill();
        ctx.closePath();
    };
    Bala.prototype.setX = function (x) {
        this.x = x;
    };
    Bala.prototype.setY = function (y) {
        this.y = y;
    };
    Bala.prototype.getX = function () {
        return this.x;
    };
    Bala.prototype.getY = function () {
        return this.y;
    };
    Bala.prototype.setFin = function () {
        this.fin = true;
    };
    Bala.prototype.esFin = function () {
        return this.fin;
    };
    Bala.prototype.visitNave1 = function (nave) {
        nave.setVida(nave.getVida() - this.getDano() - 4);
    };
    Bala.prototype.visitNave2 = function (nave) {
        nave.setVida(nave.getVida() - this.getDano() - 3);
    };
    Bala.prototype.visitNave3 = function (nave) {
        nave.setVida(nave.getVida() - this.getDano() - 2);
    };
    Bala.prototype.visitNave4 = function (nave) {
        nave.setVida(nave.getVida() - this.getDano() - 1);
    };
    return Bala;
})();
var BalaDecorador = (function () {
    function BalaDecorador(bd) {
        this.balaDecorada = bd;
    }
    BalaDecorador.prototype.draw = function (ctx) {
        this.balaDecorada.draw(ctx);
    };
    BalaDecorador.prototype.setX = function (x) {
        this.balaDecorada.setX(x);
    };
    BalaDecorador.prototype.setY = function (y) {
        this.balaDecorada.setY(y);
    };
    BalaDecorador.prototype.getX = function () {
        return this.balaDecorada.getX();
    };
    BalaDecorador.prototype.getY = function () {
        return this.balaDecorada.getY();
    };
    BalaDecorador.prototype.setFin = function () {
        this.balaDecorada.setFin();
    };
    BalaDecorador.prototype.esFin = function () {
        return this.balaDecorada.esFin();
    };
    BalaDecorador.prototype.getDano = function () {
        return this.balaDecorada.getDano();
    };
    BalaDecorador.prototype.setDano = function (d) {
        this.balaDecorada.setDano(d);
    };
    BalaDecorador.prototype.visitNave1 = function (n) {
        return this.balaDecorada.visitNave1(n);
    };
    BalaDecorador.prototype.visitNave2 = function (n) {
        return this.balaDecorada.visitNave2(n);
    };
    BalaDecorador.prototype.visitNave3 = function (n) {
        return this.balaDecorada.visitNave3(n);
    };
    BalaDecorador.prototype.visitNave4 = function (n) {
        return this.balaDecorada.visitNave4(n);
    };
    BalaDecorador.prototype.xokaEnemy = function () {
        return this.balaDecorada.xokaEnemy();
    };
    return BalaDecorador;
})();
var SuperBala = (function (_super) {
    __extends(SuperBala, _super);
    function SuperBala(db) {
        _super.call(this, db);
        this.setDano(this.getDano() + 10);
    }
    SuperBala.prototype.draw = function (ctx) {
        ctx.lineWidth = 7;
        ctx.strokeStyle = 'red';
        _super.prototype.draw.call(this, ctx);
    };
    return SuperBala;
})(BalaDecorador);
function toDeg(p) {
    if (p < 0) {
        return 360 + (p * (180 / Math.PI));
    }
    return p * (180 / Math.PI);
}
var Nave = (function () {
    function Nave(w, h, x, y, speed, rango, vida) {
        this.state = StopStateNave.getSingleton();
        this.x = x;
        this.y = y;
        this.speed = speed;
        this.vida = vida;
        this.imgSrc = [];
        this.actualImage = 0;
        this.rango = rango;
        this.width = w;
        this.height = h;
        this.anguloViejo = 0;
        this.anguloNuevo = 0;
        this.centroX = this.x + this.width / 2; //buwo 188/2
        this.centroY = this.y + this.height / 2;
        this.timerAngulo = 0;
        this.id = 0;
        this.img = new Image();
        this.T = 16;
    }
    Nave.prototype.accept = function (bala) {
    };
    Nave.prototype.animate = function () {
        this.state.animate(this);
    };
    Nave.prototype.getState = function () {
        return this.state;
    };
    Nave.prototype.setState = function (s) {
        this.state = s;
    };
    Nave.prototype.getVida = function () {
        return this.vida;
    };
    Nave.prototype.setVida = function (x) {
        this.vida = x;
    };
    Nave.prototype.drawMiBala = function (ctx) {
        if (this.getState() !== ShootingStateNave.getSingleton() || this.bala === null || this.bala.esFin()) {
            return;
        }
        else {
            this.bala.draw(ctx);
        }
    };
    Nave.prototype.shoot = function (x, y, enemy) {
        var _this = this;
        // this.setState(ShootingStateNave.getSingleton())
        if (Math.floor((Math.random() * 5) + 1) === 5) {
            this.bala = new SuperBala(new Bala(this.centroX, this.centroY, 10, enemy));
        }
        else {
            this.bala = new Bala(this.centroX, this.centroY, 10, enemy);
        }
        var lambda = function (a, b) {
            _this.bala.setX(_this.bala.getX() + a);
            _this.bala.setY(_this.bala.getY() + b);
        };
        this.helper(x, y, 20, 0, 0, lambda, function (a) {
            if (_this.bala.esFin()) {
                return false;
            }
            else if (a > _this.rango) {
                _this.bala.setFin();
                return false;
            }
            else if (_this.bala.xokaEnemy()) {
                _this.bala.setFin();
                enemy.accept(_this.bala);
                return false;
            }
            return true;
        });
    };
    Nave.prototype.nextImage = function () {
        if (this.actualImage === this.imgSrc.length - 1) {
            this.img.src = this.imgSrc[0];
            this.actualImage = 0;
        }
        else {
            this.img.src = this.imgSrc[this.actualImage + 1];
            this.actualImage += 1;
        }
    };
    Nave.prototype.setStopImage = function () {
        this.actualImage = 0;
        this.img.src = this.imgSrc[0];
    };
    Nave.prototype.setImgSrc = function (arr) {
        this.imgSrc = arr;
    };
    Nave.prototype.setCentroX = function (i) {
        this.centroX = i;
    };
    Nave.prototype.getCentroX = function () {
        return this.centroX;
    };
    Nave.prototype.setCentroY = function (i) {
        this.centroY = i;
    };
    Nave.prototype.getCentroY = function () {
        return this.centroY;
    };
    Nave.prototype.getId = function () {
        return this.id;
    };
    Nave.prototype.setId = function (id) {
        return this.id = id;
    };
    Nave.prototype.drawNaveFinal = function (ctx) {
        ctx.save();
        ctx.translate(this.centroX, this.centroY);
        ctx.rotate(toRad(this.anguloViejo));
        this.animate();
        ctx.drawImage(this.img, -this.width / 2, -this.height / 2);
        ctx.restore();
        function toRad(p) {
            return p * (Math.PI / 180);
        }
    };
    Nave.prototype.helper = function (x, y, speed, hastaX, hastaY, f, cond) {
        var _this = this;
        clearInterval(this.timerAngulo);
        var delta = 5;
        var dir = 1;
        var vectorX;
        var vectorY;
        var deltaVectorX;
        var deltaVectorY;
        var recorrido = 0;
        var r;
        var fx = Math.abs(this.centroX - x);
        var fy = Math.abs(this.centroY - y);
        var skirtumas;
        if (fx > fy)
            skirtumas = fy / fx;
        else
            skirtumas = fx / fy;
        var vel = speed;
        this.anguloNuevo = toDeg(Math.atan2(x - this.centroX, -(y - this.centroY)));
        var v = this.anguloViejo;
        var n = this.anguloNuevo;
        if (n > v) {
            if (360 - n + v < n - v) {
                dir = -1;
                r = 360 - n + v;
            }
            else {
                dir = 1;
                r = n - v;
            }
        }
        else {
            if (360 - v + n < v - n) {
                dir = 1;
                r = 360 - v + n;
            }
            else {
                dir = -1;
                r = v - n;
            }
        }
        if (x >= this.centroX) {
            vectorX = 1;
            deltaVectorX = x - this.centroX;
        }
        else {
            vectorX = -1;
            deltaVectorX = this.centroX - x;
        }
        if (y >= this.centroY) {
            vectorY = 1;
            deltaVectorY = y - this.centroY;
        }
        else {
            vectorY = -1;
            deltaVectorY = this.centroY - y;
        }
        var haCumplidoDistancia = false;
        var haCumplidoElAngulo = false;
        this.timerAngulo = setInterval(function () {
            if (r <= delta) {
                if (r === delta) {
                    delta = 0;
                }
                else
                    delta = r;
            }
            if (fx > fy) {
                if (cond(recorrido) && deltaVectorX - hastaX > 1) {
                    f(vel * vectorX, vel * skirtumas * vectorY);
                    deltaVectorX -= vel;
                    recorrido += vel;
                }
                else {
                    haCumplidoDistancia = true;
                }
            }
            else {
                if (cond(recorrido) && deltaVectorY - hastaY > 1) {
                    f(vel * skirtumas * vectorX, vel * vectorY);
                    deltaVectorY -= vel;
                    recorrido += vel;
                }
                else {
                    haCumplidoDistancia = true;
                }
            }
            if (Math.floor(_this.anguloNuevo) !== Math.floor(_this.anguloViejo)) {
                r -= delta;
                _this.anguloViejo += delta * dir;
                if (_this.anguloViejo < 0)
                    _this.anguloViejo += 360;
                else if (_this.anguloViejo > 360)
                    _this.anguloViejo -= 360;
            }
            else {
                haCumplidoElAngulo = true;
            }
            if (haCumplidoElAngulo && haCumplidoDistancia) {
                _this.setState(StopStateNave.getSingleton());
                clearInterval(_this.timerAngulo);
            }
        }, this.T);
    };
    Nave.prototype.move = function (x, y) {
        var _this = this;
        if (!x && !y)
            return;
        var lambda = function (a, b) {
            _this.centroX += a;
            _this.centroY += b;
        };
        this.helper(x, y, this.speed, this.width / 2, this.height / 2, lambda, function (a) {
            return true;
        });
    };
    return Nave;
})();
var Nave1 = (function (_super) {
    __extends(Nave1, _super);
    function Nave1() {
        _super.call(this, 129, 186, 50, 50, 3, 400, 150);
        this.setImgSrc(['images/nave10.png']);
        // this.setVida(120)
    }
    Nave1.prototype.accept = function (bala) {
        bala.visitNave1(this);
    };
    return Nave1;
})(Nave);
var Nave2 = (function (_super) {
    __extends(Nave2, _super);
    function Nave2() {
        _super.call(this, 182, 168, 600, 400, 4, 420, 130);
        this.setImgSrc(['images/nave20.png', 'images/nave21.png', 'images/nave25.png']);
        // this.setVida(100)
    }
    Nave2.prototype.accept = function (bala) {
        bala.visitNave2(this);
    };
    return Nave2;
})(Nave);
var Nave3 = (function (_super) {
    __extends(Nave3, _super);
    function Nave3() {
        _super.call(this, 89, 103, 50, 400, 5, 500, 100);
        // this.setVida(80)
        this.setImgSrc(['images/nave30.png']);
    }
    Nave3.prototype.accept = function (bala) {
        bala.visitNave3(this);
    };
    return Nave3;
})(Nave);
var Nave4 = (function (_super) {
    __extends(Nave4, _super);
    function Nave4() {
        _super.call(this, 105, 101, 600, 400, 6, 550, 80);
        this.setImgSrc(['images/nave40.png']);
        // this.setVida(60)
    }
    Nave4.prototype.accept = function (bala) {
        bala.visitNave4(this);
    };
    return Nave4;
})(Nave);
var FactoryNaves = (function () {
    function FactoryNaves() {
    }
    FactoryNaves.prototype.getNave = function (tipo) {
        if (tipo === 'nave1') {
            return new Nave1();
        }
        else if (tipo === 'nave2') {
            return new Nave2();
        }
        else if (tipo === 'nave3') {
            return new Nave3();
        }
        else if (tipo === 'nave4') {
            return new Nave4();
        }
    };
    return FactoryNaves;
})();
var ShootingStateNave = (function () {
    function ShootingStateNave() {
        if (ShootingStateNave.singleton) {
            throw new Error('Es un singleton, usa otra getInstancia() para obtener su referencia');
        }
        ShootingStateNave.singleton = this;
    }
    ShootingStateNave.getSingleton = function () {
        return ShootingStateNave.singleton;
    };
    ShootingStateNave.prototype.animate = function (n) {
        n.nextImage();
    };
    ShootingStateNave.singleton = new ShootingStateNave();
    return ShootingStateNave;
})();
var MovingStateNave = (function () {
    function MovingStateNave() {
        if (MovingStateNave.singleton) {
            throw new Error('Es un singleton, usa otra getInstancia() para obtener su referencia');
        }
        MovingStateNave.singleton = this;
    }
    MovingStateNave.getSingleton = function () {
        return MovingStateNave.singleton;
    };
    MovingStateNave.prototype.animate = function (n) {
        n.nextImage();
    };
    MovingStateNave.singleton = new MovingStateNave();
    return MovingStateNave;
})();
var StopStateNave = (function () {
    function StopStateNave() {
        if (StopStateNave.singleton) {
            throw new Error('Es un singleton, usa otra getSingleton() para obtener su referencia');
        }
        StopStateNave.singleton = this;
    }
    StopStateNave.getSingleton = function () {
        return StopStateNave.singleton;
    };
    StopStateNave.prototype.animate = function (n) {
        n.setStopImage();
    };
    StopStateNave.singleton = new StopStateNave();
    return StopStateNave;
})();
var Assets = (function () {
    function Assets() {
        this.allLoaded = false;
        this.images = ['images/nave10.png', 'images/nave20.png', 'images/nave21.png', 'images/nave22.png', 'images/nave23.png', 'images/nave24.png', 'images/nave25.png', 'images/nave30.png', 'images/nave40.png', 'images/fondo2.png',];
    }
    Assets.prototype.preload = function () {
        var _this = this;
        var alreadyLoaded = 0;
        this.images.forEach(function (a, b) {
            var im = new Image();
            im.src = a;
            var len = _this.images.length;
            im.onload = function () {
                alreadyLoaded += 1;
                if (len === alreadyLoaded) {
                    _this.allLoaded = true;
                }
            };
        });
    };
    Assets.prototype.isAllLoaded = function () {
        return this.allLoaded;
    };
    return Assets;
})();
var MiniMap = (function () {
    function MiniMap(w, h, pequeno, fondoW, fondoH, width, height) {
        this.miniMapW = w;
        this.miniMapH = h;
        this.miniMapCuadrado = pequeno;
        this.proporcionX = (this.miniMapW - this.miniMapCuadrado) / (fondoW - width);
        this.proporcionY = (this.miniMapH - this.miniMapCuadrado) / (fondoH - height);
    }
    MiniMap.prototype.drawMiniMap = function (ctx, h, deltax, deltay) {
        ctx.beginPath();
        ctx.rect(0, h - 150, 200, 150);
        ctx.fillStyle = 'grey';
        ctx.fill();
        ctx.strokeStyle = 'yellow';
        ctx.stroke();
        ctx.beginPath();
        ctx.rect(deltax * this.proporcionX, (h - this.miniMapH) + deltay * this.proporcionY, this.miniMapCuadrado, this.miniMapCuadrado);
        ctx.strokeStyle = 'red';
        ctx.stroke();
    };
    MiniMap.prototype.esSobreMiniMap = function (x, y, h) {
        if (x > 0 && x < this.miniMapW && y > h - this.miniMapH && y < h) {
            return true;
        }
        else {
            return false;
        }
    };
    return MiniMap;
})();
var ComandoMover = (function () {
    function ComandoMover(n, x, y) {
        this.nave = n;
        this.x = x;
        this.y = y;
    }
    ComandoMover.prototype.ejecutar = function () {
        if (this.nave.getState() === ShootingStateNave.getSingleton()) {
            console.log('taip jis shoting state dabar');
            return;
        }
        this.nave.setState(MovingStateNave.getSingleton());
        this.nave.move(this.x, this.y);
    };
    ComandoMover.prototype.getx = function () {
        return this.x;
    };
    ComandoMover.prototype.setx = function (x) {
        this.x = x;
    };
    ComandoMover.prototype.gety = function () {
        return this.y;
    };
    ComandoMover.prototype.sety = function (y) {
        this.y = y;
    };
    return ComandoMover;
})();
var ComandoAtacar = (function () {
    function ComandoAtacar(n, x, y, enemy) {
        this.nave = n;
        this.x = x;
        this.y = y;
        this.enemy = enemy;
    }
    ComandoAtacar.prototype.ejecutar = function () {
        this.nave.setState(ShootingStateNave.getSingleton());
        this.nave.shoot(this.x, this.y, this.enemy);
    };
    ComandoAtacar.prototype.getEnemy = function () {
        return this.enemy;
    };
    ComandoAtacar.prototype.getx = function () {
        return this.x;
    };
    ComandoAtacar.prototype.setx = function (x) {
        this.x = x;
    };
    ComandoAtacar.prototype.gety = function () {
        return this.y;
    };
    ComandoAtacar.prototype.sety = function (y) {
        this.y = y;
    };
    return ComandoAtacar;
})();
var ComandoControl = (function () {
    function ComandoControl() {
        this.commandList = [];
        this.commandList = [];
    }
    ComandoControl.prototype.add = function (c) {
        this.commandList.push(c);
    };
    ComandoControl.prototype.reset = function () {
        this.commandList = [];
    };
    ComandoControl.prototype.esVacio = function () {
        return this.commandList.length === 0;
    };
    ComandoControl.prototype.ejecutarUno = function () {
        this.commandList.shift().ejecutar();
    };
    /* estos metodos abajo se encargar de ajustar las coerdanadas cuando los jugadores mueven el mapa grande
*/
    ComandoControl.prototype.arreglarDeltaX = function (x) {
        this.commandList.forEach(function (a, b) {
            a.setx(a.getx() - x);
        });
    };
    ComandoControl.prototype.arreglarDeltaY = function (y) {
        this.commandList.forEach(function (a, b) {
            a.sety(a.gety() - y);
        });
    };
    return ComandoControl;
})();
///<reference path="Bala.ts"/>
///<reference path="naveModule.ts"/>
///<reference path="Assets.ts"/>
///<reference path="MiniMap.ts"/>
///<reference path="CommandPattern.ts"/>
var Game = (function () {
    function Game(miTipoNave, enemyTipoNave) {
        // console.log('konstructorius su miTipo ' + miTipoNave + ' enemy ' + enemyTipoNave)
        this.canvas = document.createElement('canvas');
        this.ctx = this.canvas.getContext("2d");
        document.body.appendChild(this.canvas);
        this.w = this.canvas.width = window.innerWidth;
        this.h = this.canvas.height = window.innerHeight;
        // console.log('constructorius')
        this.dirx = 0;
        this.diry = 0;
        this.deltax = 0;
        this.deltay = 0;
        this.zoomX = 5;
        this.zoomY = 5;
        this.fondoW = 3000;
        this.fondoH = 1500;
        this.ratonMargin = 20;
        this.minimap = new MiniMap(200, 150, 50, this.fondoW, this.fondoH, this.w, this.h);
        this.T = 16; //el periodo de la frencuenta 60 frame/second
        this.animationLock = false;
        this.animationTimer;
        this.background = new Image();
        // this.canvas.id = 'canvas' //css id for canvas
        this.background.src = 'images/fondo2.png';
        this.timerDireccion;
        this.factoriaDeNaves = new FactoryNaves();
        this.miNave = this.factoriaDeNaves.getNave(miTipoNave);
        this.enemyNave = this.factoriaDeNaves.getNave(enemyTipoNave);
        // this.miNave = new Nave(miTipoNave)
        // this.enemyNave = new Nave(enemyTipoNave)
        this.comandosPendientes = new ComandoControl();
        this.comandosPendientesEnemy = new ComandoControl();
    }
    Game.prototype.getMiniMap = function () {
        return this.minimap;
    };
    Game.prototype.getMiNave = function () {
        return this.miNave;
    };
    Game.prototype.getEnemyNave = function () {
        return this.enemyNave;
    };
    Game.prototype.getCanvas = function () {
        return this.canvas;
    };
    Game.prototype.setCanvas = function (c) {
        this.canvas = c;
    };
    Game.prototype.getCtx = function () {
        return this.ctx;
    };
    Game.prototype.esSobreMiniMapa = function (x, y) {
        this.getMiniMap().esSobreMiniMap(x, y, this.h);
    };
    Game.prototype.drawMiNave = function () {
        // console.log('mi vida ' + this.getMiNave().getVida())
        this.miNave.drawNaveFinal(this.ctx);
    };
    Game.prototype.drawEnemyNave = function () {
        // console.log('su vida ' + this.getMiNave().getVida())
        this.enemyNave.drawNaveFinal(this.ctx);
    };
    Game.prototype.anadirMove = function (x, y) {
        this.comandosPendientes.add(new ComandoMover(this.getMiNave(), x, y));
    };
    Game.prototype.anadirMoveEnemy = function (x, y) {
        this.comandosPendientesEnemy.add(new ComandoMover(this.getEnemyNave(), x, y));
    };
    Game.prototype.anadirShoot = function (x, y, destinyNave) {
        this.comandosPendientes.add(new ComandoAtacar(this.getMiNave(), x, y, destinyNave));
    };
    Game.prototype.anadirShootEnemy = function (x, y, destinyNave) {
        this.comandosPendientesEnemy.add(new ComandoAtacar(this.getEnemyNave(), x, y, destinyNave));
    };
    Game.prototype.moveInmediatamente = function (x, y) {
        new ComandoMover(this.getMiNave(), x, y).ejecutar();
    };
    Game.prototype.moveInmediatamenteEnemy = function (x, y) {
        new ComandoMover(this.getEnemyNave(), x, y).ejecutar();
    };
    Game.prototype.shootInmediatamente = function (x, y, destinyNave) {
        new ComandoAtacar(this.getMiNave(), x, y, destinyNave).ejecutar();
    };
    Game.prototype.shootInmediatamenteEnemy = function (x, y, destinyNave) {
        new ComandoAtacar(this.getEnemyNave(), x, y, destinyNave).ejecutar();
    };
    Game.prototype.limpiarComandosPendientes = function () {
        this.comandosPendientes.reset();
    };
    Game.prototype.limpiarComandosPendientesEnemy = function () {
        this.comandosPendientesEnemy.reset();
    };
    Game.prototype.checkForActions = function () {
        if (this.getMiNave().getState() === StopStateNave.getSingleton() && !this.comandosPendientes.esVacio()) {
            // this.getMiNave().setState(MovingStateNave.getSingleton())
            this.comandosPendientes.ejecutarUno();
        }
        if (this.getEnemyNave().getState() === StopStateNave.getSingleton() && !this.comandosPendientesEnemy.esVacio()) {
            // this.getEnemyNave().setState(MovingStateNave.getSingleton())
            this.comandosPendientesEnemy.ejecutarUno();
        }
    };
    Game.prototype.drawBackground = function () {
        this.w = this.canvas.width = this.canvas.width;
        this.h = this.canvas.height = this.canvas.height;
        this.ctx.drawImage(this.background, this.deltax, this.deltay, this.w, this.h, 0, 0, this.w, this.h);
    };
    Game.prototype.drawMiniMap = function () {
        this.minimap.drawMiniMap(this.ctx, this.h, this.deltax, this.deltay);
    };
    Game.prototype.moverPantalla = function (e) {
        //si esta encima del minimapa
        var x = e.pageX;
        var y = e.pageY;
        //x cordenadas
        if (x > this.ratonMargin && x < this.w - this.ratonMargin && y > this.ratonMargin && y < this.h - this.ratonMargin) {
            clearInterval(this.animationTimer);
            this.animationLock = false;
        }
        else {
            if (!this.animationLock) {
                this.animationLock = true;
                var that = this;
                this.animationTimer = setInterval(function () {
                    that.hacerCambioRelativos();
                }, that.T);
            }
            //cada lado
            if (x < this.ratonMargin) {
                this.dirx = -1;
            }
            else if (x > this.w - this.ratonMargin) {
                this.dirx = 1;
            }
            else if (x > this.ratonMargin) {
                this.dirx = 0;
            }
            if (y < this.ratonMargin) {
                this.diry = -1;
            }
            else if (y > this.h - this.ratonMargin) {
                this.diry = 1;
            }
            else if (y > this.ratonMargin) {
                this.diry = 0;
            }
        }
    };
    Game.prototype.getDeltaX = function () {
        return this.deltax;
    };
    Game.prototype.getDeltaY = function () {
        return this.deltay;
    };
    Game.prototype.hacerCambioRelativos = function () {
        // console.log('cia yra this ' + this.miNave)
        var aumentarX, aumentarY;
        if (this.dirx !== 0) {
            aumentarX = this.fondoW - this.w - this.deltax;
            if (this.deltax <= this.zoomX && this.dirx === -1) {
                this.deltax -= this.deltax;
                // this.miNave.centroX += this.deltax
                this.miNave.setCentroX(this.miNave.getCentroX() + this.deltax);
                this.enemyNave.setCentroX(this.enemyNave.getCentroX() + this.deltax);
                this.comandosPendientes.arreglarDeltaX(this.deltax);
                this.comandosPendientesEnemy.arreglarDeltaX(this.deltax);
            }
            else if (this.zoomX >= aumentarX && this.dirx === 1) {
                this.deltax += aumentarX;
                // this.miNave.centroX -= aumentarX
                this.miNave.setCentroX(this.miNave.getCentroX() - aumentarX);
                this.enemyNave.setCentroX(this.enemyNave.getCentroX() - aumentarX);
                this.comandosPendientes.arreglarDeltaX(aumentarX);
                this.comandosPendientesEnemy.arreglarDeltaX(aumentarX);
            }
            else {
                this.deltax += this.zoomX * this.dirx;
                // this.miNave.centroX -= this.zoomX * this.dirx
                // console.log(this.miNave)
                this.miNave.setCentroX(this.miNave.getCentroX() - this.zoomX * this.dirx);
                this.enemyNave.setCentroX(this.enemyNave.getCentroX() - this.zoomX * this.dirx);
                this.comandosPendientes.arreglarDeltaX(this.zoomX * this.dirx);
                this.comandosPendientesEnemy.arreglarDeltaX(this.zoomX * this.dirx);
            }
        }
        if (this.diry !== 0) {
            aumentarY = this.fondoH - this.h - this.deltay;
            if (this.deltay <= this.zoomY && this.diry === -1) {
                this.deltay -= this.deltay;
                // this.miNave.centroY += this.deltay
                this.miNave.setCentroY(this.miNave.getCentroY() + this.deltay);
                this.enemyNave.setCentroY(this.enemyNave.getCentroY() + this.deltay);
                this.comandosPendientes.arreglarDeltaY(this.deltay);
                this.comandosPendientesEnemy.arreglarDeltaY(this.deltay);
            }
            else if (this.zoomY >= aumentarY && this.diry === 1) {
                this.deltay += aumentarY;
                // this.miNave.centroY -= aumentarY
                this.miNave.setCentroY(this.miNave.getCentroY() - aumentarY);
                this.enemyNave.setCentroY(this.enemyNave.getCentroY() - aumentarY);
                this.comandosPendientes.arreglarDeltaY(aumentarY);
                this.comandosPendientesEnemy.arreglarDeltaY(aumentarY);
            }
            else {
                this.deltay += this.zoomY * this.diry;
                // this.miNave.centroY -= this.zoomY * this.diry
                this.miNave.setCentroY(this.miNave.getCentroY() - this.zoomY * this.diry);
                this.enemyNave.setCentroY(this.enemyNave.getCentroY() - this.zoomY * this.diry);
                this.comandosPendientes.arreglarDeltaY(this.zoomY * this.diry);
                this.comandosPendientesEnemy.arreglarDeltaY(this.zoomY * this.diry);
            }
        }
    };
    Game.prototype.drawMiBala = function () {
        this.getMiNave().drawMiBala(this.ctx);
    };
    Game.prototype.drawEnemyBala = function () {
        this.getEnemyNave().drawMiBala(this.ctx);
    };
    Game.prototype.mostrarPuntos = function () {
        // this.ctx.font="30px Verdana"
        this.ctx.font = "15px Georgia";
        this.ctx.fillStyle = 'green';
        this.ctx.fillText("Mi Vida: " + this.getMiNave().getVida(), 0, 15);
        this.ctx.fillText("Su Vida: " + this.getEnemyNave().getVida(), 0, 30);
    };
    Game.prototype.startGame = function () {
        var _this = this;
        var assets = new Assets();
        assets.preload();
        var preloadStuff = function () {
            if (assets.isAllLoaded()) {
                // document.body.appendChild(game.getCanvas())
                _this.render();
            }
            else {
                setTimeout(preloadStuff, 50);
            }
        };
        preloadStuff();
    };
    Game.prototype.render = function () {
        var _this = this;
        this.timerGame = setInterval(function () {
            _this.drawBackground();
            _this.drawMiNave();
            _this.drawEnemyNave();
            _this.drawMiniMap();
            _this.checkForActions();
            _this.drawMiBala();
            _this.drawEnemyBala();
            _this.mostrarPuntos();
        }, this.T);
    };
    return Game;
})();
///<reference path="Game.ts"/>
// var socket = io.connect('http://localhost:9000')
var socket = io.connect('http://52.28.107.172:9000');
//main method :D
window.onload = function () {
    var miTipoNave;
    var enemyTipoNave;
    var game;
    var puedeAtacar = true;
    document.getElementById('elegirNave').onclick = function (e) {
        var tipoNave = e.target.id;
        if (tipoNave === 'elegirNave')
            return;
        miTipoNave = tipoNave;
        socket.emit('elegirNave', tipoNave);
        document.getElementById('elegirNave').style.display = 'none';
        document.getElementById('msgWaiting').style.display = 'block';
    };
    //------------------ bunch of listeners ------------------------------------
    socket.on('startGame', function (data) {
        enemyTipoNave = data;
        game = new Game(miTipoNave, enemyTipoNave);
        document.getElementById('msgWaiting').style.display = 'none';
        game.startGame();
        socket.on('leftClick', function (data) {
            var x = data[0] - game.getDeltaX();
            var y = data[1] - game.getDeltaY();
            if (data[2] === game.getMiNave().getId()) {
                if (game.esSobreMiniMapa(x, y)) {
                    return;
                }
                game.limpiarComandosPendientes();
                game.shootInmediatamente(x, y, game.getEnemyNave());
            }
            else {
                game.limpiarComandosPendientesEnemy();
                game.shootInmediatamenteEnemy(x, y, game.getMiNave());
            }
        });
        socket.on('Shift-LeftClick', function (data) {
            var x = data[0] - game.getDeltaX();
            var y = data[1] - game.getDeltaY();
            if (data[2] === game.getMiNave().getId()) {
                if (game.esSobreMiniMapa(x, y)) {
                    return;
                }
                // console.log('recibido shift left')
                game.anadirShoot(x, y, game.getEnemyNave());
            }
            else {
                game.anadirShootEnemy(x, y, game.getMiNave());
            }
        });
        socket.on('rightClick', function (data) {
            var x = data[0] - game.getDeltaX();
            var y = data[1] - game.getDeltaY();
            if (data[2] === game.getMiNave().getId()) {
                if (game.esSobreMiniMapa(x, y)) {
                    return;
                }
                game.limpiarComandosPendientes();
                game.moveInmediatamente(x, y);
            }
            else {
                game.limpiarComandosPendientesEnemy();
                game.moveInmediatamenteEnemy(x, y);
            }
        });
        socket.on('Shift-RightClick', function (data) {
            var x = data[0] - game.getDeltaX();
            var y = data[1] - game.getDeltaY();
            if (data[2] === game.getMiNave().getId()) {
                if (game.esSobreMiniMapa(x, y)) {
                    return;
                }
                game.anadirMove(x, y);
            }
            else {
                game.anadirMoveEnemy(x, y);
            }
        });
        socket.on('miId', function (data) {
            game.getMiNave().setId(data);
        });
        socket.on('enemyId', function (data) {
            game.getEnemyNave().setId(data);
        });
        document.onmousemove = function (e) {
            game.moverPantalla(e);
        };
        window.oncontextmenu = function () {
            return false;
        };
        document.onmouseup = function (e) {
            var x = e.pageX + game.getDeltaX();
            var y = e.pageY + game.getDeltaY();
            var qBotton = e.which;
            var ataca = function (clickHexo) {
                puedeAtacar = false;
                socket.emit(clickHexo, [x, y]);
                setTimeout(function () {
                    puedeAtacar = true;
                }, 3000);
            };
            if (qBotton === 3 && e.shiftKey) {
                socket.emit('Shift-RightClick', [x, y]);
            }
            else if (qBotton === 1 && e.shiftKey) {
                if (puedeAtacar) {
                    ataca('Shift-LeftClick');
                }
                else {
                }
            }
            else if (qBotton === 3) {
                socket.emit('rightClick', [x, y]);
            }
            else if (qBotton === 1) {
                if (puedeAtacar) {
                    ataca('leftClick');
                }
                else {
                }
            }
        };
    });
};
