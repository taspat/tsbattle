var app = require('http').createServer(handler)
var io = require('socket.io')(app)
var fs = require('fs')
var url = require('url')
var path = require('path')
var players = []

app.listen(9000)

function handler (req, res) {
  var ext = path.extname(url.parse(req.url).pathname)
  // console.log(ext)
  var fichero = url.parse(req.url).pathname,
      ficheroType
  if (fichero === '/') fichero = '/index.html'
  else if (fichero === '.js') ficheroType = {'Content-Type': 'application/javascript'}
  else if (fichero === '.js') ficheroType = {'Content-Type': 'image/png'}
  else if (fichero === '.png') ficheroType = {'Content-Type': 'image/png'}
  else if (fichero === '.css') ficheroType = {'Content-Type': 'text/css'}
  fs.readFile(__dirname + fichero,
              function (err, data) {
                if (err) {
                  res.writeHead(500, ficheroType);
                  return res.end('Error loading index.html');
                }
                res.writeHead(200);
                res.end(data);
              });
}

io.on('connection', function (socket) {
  if (players.length === 2) {
    return
  }
  socket.on('elegirNave', function(data){
    // console.log('atsiunte sita sms ' + data)
    players.push({socket : socket, id : socket.id, tipoNave : data})
    if (players.length === 2) {
      // console.log('si')
      players[0].socket.emit('startGame', players[1].tipoNave) //de paso envio tipoDeNave de eemy
      players[1].socket.emit('startGame', players[0].tipoNave)
      players[0].socket.emit('miId', players[0].id)
      players[1].socket.emit('miId', players[1].id)
      players[0].socket.emit('enemyId', players[1].id)
      players[1].socket.emit('enemyId', players[0].id)
    }
  })
  socket.on('rightClick', function (data) {
    data.push(socket.id)
    io.sockets.emit('rightClick', data)
  });
  socket.on('Shift-RightClick', function (data) {
    data.push(socket.id)
    io.sockets.emit('Shift-RightClick', data)
  });

  socket.on('Shift-LeftClick', function (data) {
    data.push(socket.id)
    io.sockets.emit('Shift-LeftClick', data)
  });
  socket.on('leftClick', function (data) {
    data.push(socket.id)
    io.sockets.emit('leftClick', data)
  });

  socket.on('disconnect', function (data) {
    // console.log(players)
    for (var i = 0; i < players.length; i++) {
      if (players[i].socket === socket) {
        players.splice(i)
      }
    }
    // console.log(players)
  });
  // console.log(players.length)
});
