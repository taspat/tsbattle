class Assets {
    private images: string[]
    private allLoaded: boolean
    constructor() {
        this.allLoaded = false
        this.images = ['images/nave10.png',
                       'images/nave20.png',
                       'images/nave21.png',
                       'images/nave22.png',
                       'images/nave23.png',
                       'images/nave24.png',
                       'images/nave25.png',
                       'images/nave30.png',
                       'images/nave40.png',
                       'images/fondo2.png',]
    }
    public preload() {
        var alreadyLoaded: number = 0
        this.images.forEach((a,b) => {
            var im = new Image()
            im.src = a
            var len = this.images.length
            im.onload = () => {
                alreadyLoaded += 1
                if (len === alreadyLoaded) {
                    this.allLoaded = true
                }
            }
        })
    }
    public isAllLoaded() {
        return this.allLoaded
    }
    
}
