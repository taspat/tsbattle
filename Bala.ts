interface IBala {
    draw(ctx)
    setX(x)
    setY(y) 
    getX()
    getY()
    setFin()
    esFin()
    visitNave1(n: Nave1)
    visitNave2(n: Nave2) 
    visitNave3(n: Nave3)
    visitNave4(n: Nave4) 
    xokaEnemy()
    getDano()
    setDano(d)
}

class Bala implements IBala{
    private x: number //destino x
    private y: number
    private dano: number
    private fin:boolean
    private enemy
    constructor (x,y,dano,n) {
        this.x = x
        this.y = y
        this.dano = dano
        this.fin = false
        this.enemy = n
    }
    public getDano(){
        return this.dano
    }
    public setDano(d){
        this.dano = d
    }
    public xokaEnemy(){
        var dx = this.x - this.enemy.getCentroX()
        var dy = this.y - this.enemy.getCentroY()
        return dx*dx+dy*dy < 3000
    }
    public draw (ctx) {
        ctx.beginPath(); 
        ctx.arc(this.getX(),this.getY(),5,0,2*Math.PI)
        ctx.fillStyle = 'yellow'
        ctx.stroke()
        ctx.fill()
        ctx.closePath(); 
    }
    public setX(x) {
        this.x = x
    }
    public setY(y) {
        this.y = y
    }
    public getX(){
        return this.x
    }
    public getY(){
        return this.y
    }
    public setFin(){
        this.fin = true
    }
    public esFin(){
        return this.fin
    }
    public visitNave1(nave: Nave1) {
        nave.setVida(nave.getVida()-this.getDano()-4)
    }
    public visitNave2(nave: Nave2) {
        nave.setVida(nave.getVida()-this.getDano()-3)
    }
    public visitNave3(nave: Nave3) {
        nave.setVida(nave.getVida()-this.getDano()-2)
    }
    public visitNave4(nave: Nave4) {
        nave.setVida(nave.getVida()-this.getDano()-1)
    }
}


class BalaDecorador implements IBala{
    private balaDecorada: IBala

    constructor (bd: IBala) {
        this.balaDecorada = bd
    }
    public draw(ctx){
        this.balaDecorada.draw(ctx)
    }
    public setX(x) {
        this.balaDecorada.setX(x)
    }
    public setY(y) {
        this.balaDecorada.setY(y)
    }
    public getX() {
        return this.balaDecorada.getX()
    }
    public getY(){
        return this.balaDecorada.getY()
    }
    public setFin(){
        this.balaDecorada.setFin()
    }
    public esFin(){
        return this.balaDecorada.esFin()
    }
    public getDano(){
        return this.balaDecorada.getDano()
    }
    public setDano(d){
        this.balaDecorada.setDano(d)
    }
    public visitNave1(n:Nave1){
        return this.balaDecorada.visitNave1(n)
    }
    public visitNave2(n:Nave2){
        return this.balaDecorada.visitNave2(n)
    }
    public visitNave3(n:Nave3){
        return this.balaDecorada.visitNave3(n)
    }
    public visitNave4(n:Nave4){
        return this.balaDecorada.visitNave4(n)
    }
    public xokaEnemy(){
        return this.balaDecorada.xokaEnemy()
    }
}

class SuperBala extends BalaDecorador {
    constructor (db: IBala) {
        super(db)
        this.setDano(this.getDano() + 10)
    }
    public draw(ctx) {
        ctx.lineWidth = 7
        ctx.strokeStyle = 'red'
        super.draw(ctx)
    }
}

